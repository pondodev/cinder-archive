String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

String.prototype.rotpos = function(start, reverse=false) {
    var obf = this.trim();
    var out = "";
    for (var i = 0; i < obf.length; i++) {
        var c = obf.charAt(i);
        var o = start + i;
        var bounds;

        if (c.match(/[a-z]/g)) bounds = [97, 122];
        else if (c.match(/[A-Z]/g)) bounds = [65, 90];
        else {
		out += c;
		continue;
	}

        var r = c.charCodeAt(0) + (reverse ? -o : o);
        while (r < bounds[0]) r = r + 26;
        while (r > bounds[1]) r = r - 26;
	out += String.fromCharCode(r);
    }
    return out;
}

function toggleload() {
    var navs = document.querySelectorAll("[data-main-toggle]");
    var anchor = window.location.pathname.substr(1);

	for (var i in navs) if (navs.hasOwnProperty(i)) {
        if (navs[i].getAttribute("data-main-toggle") == anchor) {
            navs[i].onclick();
            return;
        }
    }

    navs[0].onclick();
}

function toggleclick() {
    var mainSections = document.getElementsByTagName("main");
	var navButtons = document.querySelectorAll("[data-main-toggle]");
    var targetClass = this.getAttribute("data-main-toggle");

    for (i = 0; i < mainSections.length; ++i) {
        var visible = mainSections[i].classList.contains(targetClass);
        mainSections[i].style.display = visible ? "block" : "none";
    }

	for (i = 0; i < navButtons.length; ++i) {
        if (navButtons[i].classList.contains("active")) {
            navButtons[i].classList.remove("active");
        }
    }

    var location = "/";
    if (targetClass != "games") {
        location += targetClass;
    }

    try {
        history.pushState("", document.title, location);
    } catch (error) {}

    this.classList.add("active");
}

function miniclick() {
    var currentSelection = document.getElementsByClassName("selected");
    currentSelection[0].classList.remove("selected");
    this.classList.add("selected");
    var game = this.dataset.game;

    var descriptions = document.getElementsByClassName("mini-description")[0].children;
    console.log(descriptions);
    for (i = 0; i < descriptions.length; ++i) {
        if (descriptions[i].dataset.game == game) {
            descriptions[i].classList.add("active");
        }
        else {
            if (descriptions[i].classList.contains("active")) {
                descriptions[i].classList.remove("active");
            }
        }
    }
}

function minimoji() {
    var emojiList = [
        "&#x2728;",                             // sparkles
        "&#x1F496;",                            // sparkle heart
        "&#x2615;",                             // coffee
        "&#x1F9D9;&#x200D;&#x2640;&#xFE0F;",    // witch
        "&#x1F469;&#x200D;&#x1F4BB;",           // technologist
        "&#x1F3AE;",                            // controller
        "&#x1F389;",                            // party popper
        "&#x1F361;",                            // dango
        "&#x1F52E;",                            // crystal ball
        "&#x2604;",                             // comet ball
    ];

    var current = this.getAttribute("data-emoji");

    while (true) {
        var randIndex = Math.floor(Math.random() * emojiList.length);
        if (randIndex != current) {
            break;
        }
    }

    this.setAttribute("data-emoji", randIndex);
    this.innerHTML = emojiList[randIndex];

    twemoji.parse(document.body, {
        folder: 'svg',
        ext: '.svg'
      });
}

document.addEventListener("DOMContentLoaded", function() {
    var mojis = document.querySelectorAll(".minimoji");
    mojis[0].onclick = minimoji;
    mojis[0].click();
    /*
    for (var i in mojis) {
        mojis[i].onclick = minimoji;
        mojis[i].click();
    }
    */

	// throughout this, i will use "milk" rather than "mail".
	// this is to stop scrapers. it shouldn't be necessary, but
	// better safe than sorry eh?
	var milks = document.querySelectorAll("[data-milk]");

	var prefix = "apyclh:".rotpos(14, true);

	for (var i in milks) if (milks.hasOwnProperty(i)) {
		var encryptedMilk = milks[i].getAttribute("data-milk");
		milks[i].href = prefix + encryptedMilk.rotpos(9, true);
		milks[i].removeAttribute("data-milk");
    }

    var navs = document.querySelectorAll("[data-main-toggle]");

	for (var i in navs) if (navs.hasOwnProperty(i)) {
        navs[i].onclick = toggleclick;
    }

    toggleload();
    
    var minis = document.querySelectorAll(".mini-holder");
    for (var i in minis) {
        minis[i].onclick = miniclick;
    }

});
