# cyndr.dev
unfortunately cinder's domain and website seems to have already lapsed, but the wayback machine has several backups of
it. i used [wayback-machine-downloader](https://github.com/hartator/wayback-machine-downloader) with
[this patch](https://gist.github.com/nicholascc/a2816dc444c216ed5cc7a18b409f0823) discussed on
[this issue](https://github.com/hartator/wayback-machine-downloader/issues/265) to pull down all the assets.

> i'm hoping to also get some earlier versions of her site archived here too, including some early development versions
> of it that she shared with me when we were both working on our sites. i remember we spent a lot of time going back
> and forth with each other, sharing what we had come up with while chatting in a discord call. i don't think she really
> needed my help or input, she was always much better at web and design than i was, but she still always asked for my
> input anyway. she ended up helping me lot with my site, giving me pointers on how to structure my page so it's more
> readable and appealing. it went a long way towards not only making my site not look terrible, but also teaching me
> about ui/ux design.
>
> \- dan

## todo
- get each backup to work on their own
- prune out backups where nothing changed from a previous version

