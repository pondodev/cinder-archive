# ![gboop](gboop.svg)

gb emulator written in rust!

## documentation

- http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf
- https://gekkio.fi/files/gb-docs/gbctr.pdf
- https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
- https://gbdev.io/pandocs/

## useful crates

- https://github.com/Gekkio/imgui-rs
- https://docs.rs/winit/0.22.1/winit/
- https://crates.io/crates/gfx

## useful links

- https://cturt.github.io/cinoop.html

> this is the first big emulation project i undertook ever, and cinder was the guiding hand through this while we worked
> on it together. i was incredibly lost and nervous working on this with her, because i felt like i was nowhere near her
> skill level to be working with her on such a complicated project. but she was kind to me and encouraged me to really
> have a go at it. and so i did that, learned a lot, and even added this to my portfolio in it's unfinished state. i
> don't think i would have been able to get my current job as an embedded software engineer if not for this project.
> it's a shame we never finished it, maybe one day i'll try to find the will to get it into a working state so i can say
> we finished it.
>
> \- dan

