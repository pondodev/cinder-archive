use std::fs;
use std::io::Read;

// TODO: add MBC chip type
pub struct Cart {
    buffer: Vec<u8>,
    ram: Vec<u8>,
    pub cgb_mode: bool
}

impl Cart {
    // TODO: will only support loading MBC1 ROMs for the moment
    /// Cartridge memory mapping is as follows:
    ///
    /// Header
    /// 0x0100-0x0103 -> Entry point
    ///   Usually a NOP followed by a JP 0x0150 (00C3 5001)
    /// 0x0104-0x0133 -> Nintendo logo (not important)
    /// 0x0134-0x013E -> Title
    ///   Uppercase ASCII encoded characters of the game title. Any left over space is zeroed out
    ///   Note that on older cartridges this actually extends to 0x0143, lacking the next two things
    /// 0x013F-0x142 -> Manufacturer code (not important)
    /// 0x0143 -> CGB flag
    ///   Set to 0x80 if there is CGB support with backwards compatibility to the GameBoy
    ///   Set to 0xC0 if there is CGB support with no compatibility to the GameBoy
    /// 0x0144-0x0145 -> Publisher code (not important)
    /// 0x0146 -> SGB flag (not important unless we really want SGB feature support)
    /// 0x0147 -> Cartridge type
    ///   Specifies which MBC is used on this cartridge + any external hardware
    /// 0x0148 -> ROM size
    /// 0x0149 -> Cartridge RAM size
    /// 0x014A -> Destination code (not important)
    /// 0x014B -> Old publisher code (not important)
    /// 0x014C -> Game version number (not important)
    /// 0x014D -> Header checksum
    /// 0x014E-0x014F -> Global checksum (not important)
    pub fn new(path: &str) -> Result<Self, String> {
        let mut file = match fs::File::open(path) {
            Ok(f) => f,
            Err(_) => return Err(format!("failed to load rom: {}", path)),
        };
        let mut buffer = Vec::<u8>::new();
        match file.read_to_end(&mut buffer) {
            Ok(_) => (),
            Err(e) => return Err(e.to_string())
        }

        let ram_size = match buffer[0x0149] {
            0x00 => 0,
            0x01 => 2   * 1024,
            0x02 => 8   * 1024,
            0x03 => 32  * 1024,
            0x04 => 128 * 1024,
            0x05 => 64  * 1024,
            _    => return Err("unrecognised cart ram size".to_string())
        };
        let ram = vec![0; ram_size];
        let cgb_mode = buffer[0x0143] == 0xC0;

        // TODO: read in value at 0x0147. if != 0x00 or 0x01 return an error, else continue as normal
        // in future we would actually handle other MBC chips

        Ok(Cart {
            buffer,
            ram,
            cgb_mode
        })
    }

    pub fn get_title(&self) -> String {
        let mut title = String::new();

        for i in 0x0134..=0x0143 {
            if self.buffer[i] == 0x00 {
                break;
            }
            title.push(self.buffer[i] as char);
        }

        title
    }

    pub fn read_buffer_byte(&self, ptr: usize) -> u8 {
        // TODO: handle banking
        self.buffer[ptr]
    }

    pub fn read_ram_byte(&self, ptr: usize) -> u8 {
        // TODO: handle banking
        self.ram[ptr]
    }

    pub fn write_ram_byte(&mut self, ptr: usize, value: u8) {
        // TODO: handle banking
        self.ram[ptr] = value;
    }
}