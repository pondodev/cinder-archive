use std::env;

pub mod tests;
pub mod helper;

pub mod core;
pub mod registers;
pub mod memory;
pub mod rom;

use crate::core::*;
use crate::rom::Cart;

fn main() {
    let mut cpu = CPU::new();

    // get rom path from args and load
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("no ROM path specified");
    }
    let game_cart = match Cart::new(&args[1]) {
        Ok(gc) => gc,
        Err(e) => panic!(format!("error loading game cart: {}", e)),
    };
    if game_cart.cgb_mode {
        panic!("no support for cgb only carts");
    }

    // print out game title
    println!("loaded rom: {}", game_cart.get_title());

    'main: loop {
        cpu.step();
    }
}
