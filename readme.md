# cinder archive
this repo is dedicated to archiving anything we can get our hands on that is related to our dear friend, cinder
foster-smith. things she created, things she was involved in, things that were important to her, anything at all.

for now we'll just be collecting anything we can get our hands on and storing it in this repo, but eventually we'd like
to host this all on a site so that people can easily browse it all and remember all the things she was and continues to
be to her friends and family.

## contributing
if you have something you'd like to contribute to this archive, either submit a merge request with the stuff you'd like
archived, or contact myself or one of the other maintainers:

- [dan coady](https://www.pondo.dev/) (@pondodev)
- [cat flynn](https://ktyl.dev/) (@ktyldev)
- @Kayomn

