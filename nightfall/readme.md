# nightfall

> cinder and i had some pretty grand plans to create a podcast over the course of the 2021 lockdowns, just like everyone
> else and their dog. we were gonna call it nightfall cafe, and it was gonna be us exploring art we enjoyed. the idea
> was each episode would be us meeting in this cafe to catch up, and while catching up we would share a piece of art we
> enjoyed and talk about why we enjoyed it so much. we only ever recorded one episode of it which i edited for us. i
> forget what i spoke about, but i remember very distinctly that she spoke about le petit prince. she just loved how
> mature a love story it is, one that is more mature than most media we see these days, and it manages this in a
> childrens book. i'll have to see if i can dig up the recordings and the edited episode when i get a chance.
>
> this isn't the podcast itself though, this is the site we (read: she) came up with for the podcast. after all, we
> couldn't have a podcast and no website to go with it! i seem to remember it going further than this, with us sitting
> in a discord call while she streamed her screen to me so i could give my opinion as she tinkered with it. wherever it
> is though, i don't think i've got it, but i'm glad we at least have this left over.
>
> \- dan

